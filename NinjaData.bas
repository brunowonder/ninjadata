﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=7.31
@EndOfDesignText@
Private Sub Class_Globals
    Public  Indent       As Int
    Public  IgnoreErrors As Boolean
    Public  ShowWarnings As Boolean
    ' - - - - - - - - - - - - - - - - - -
    Private source       As Object
    Private parser       As JSONParser
    Private generator    As JSONGenerator
    ' - - - - - - - - - - - - - - - - - -
    Private Const                _
        NOT_FOUND =              _
        "%!@KEY~NOT~FOUND@!%"    _
    As String
End Sub

#Region Public
'Initializes the object based on the given input source.
'The accepted source types are:
'- Maps, lists, arrays, custom types and primitive types
'- JSON strings
Public Sub Initialize(src As Object) As Object
    Indent = 4
    Select True
        Case src Is NinjaData
            Dim obj = src As NinjaData
            source = obj.Get("/")

        Case src Is String
            source = LoadString(src)

        Case Else
            source = src

    End Select
    Return source
End Sub

'Reloads the entire object as a JSON string.
'WARNING: Any complex objects held in this data structure
'(such as arrays or custom types) will be converted to
'plain text before the reload operation takes place.
Public Sub ReloadAsJSONString As Object
    source = LoadString(JSONString(source))
    Return source
End Sub

'Returns a human readable textual representation
'of the entire object in JSON format.
Public Sub getPrettyJSON As String
    Return PrettyJSONFrom("/")
End Sub

'Returns a compact textual representation
'of the entire object in JSON format.
Public Sub getCompactJSON As String
    Return CompactJSONFrom("/")
End Sub

'Returns a human readable textual representation of the
'object existing at the given path in JSON format.
Public Sub PrettyJSONFrom(path As String) As String
    Dim out As String
    Dim data = JSONString(Get(path)) As String
    parser.Initialize(data)
    Select True
        Case data.StartsWith("{")
            generator.Initialize(parser.NextObject)
            out = generator.ToPrettyString(Indent)

        Case data.StartsWith("[")
            generator.Initialize2(parser.NextArray)
            out = generator.ToPrettyString(Indent)

        Case Else
            out = data

    End Select
    Return out
End Sub

'Returns a compact textual representation of the
'object existing at the given path in JSON format.
Public Sub CompactJSONFrom(path As String) As String
    Return JSONString(Get(path))
End Sub

'Retrives the existing object at the given path.
'Examples:
'<code>
'Get("/") 'Get root
'Get("/books/programming") 'Get object by map key
'Get("/items/3") 'Get object by list index'</code>
Public Sub Get(path As String) As Object
    Return GetObject(source, path)
End Sub

'Creates an object and/or modifies its content at the given location.
'Examples:
'<code>
'Set("/items/1", Null) 'Modifies the content held by the second object in the 'items' list
'Set("/world/player/job", "ninja") 'Modifies the content held by the 'job' key in the 'player' map
'Set("/", "bananas") 'Destroys the existing object by turning it into "bananas"'</code>
Public Sub Set(path As String, value As Object) As Object
    If value Is String Then    value = LoadString(value)
    Return SetObject(source, path, value)
End Sub

'Adds an object to an existing list.
'WARNING: Performing this operation on an array will convert it into a list!
'Examples:
'<code>
'Push("/items/", "bananas") 'Add the string "bananas" to the 'items' list
'Push("/items/", "{}") 'Add an empty map to the 'items' list
'Push("/items/", myObj) 'Add an object to the 'items' list'</code>
Public Sub Push(path_to_list As String, value As Object) As Object
    Dim obj = Get(path_to_list) As Object
    If obj == Null Then Return NotListError(obj)
    If IsArray(obj) Then
        obj = LoadString(JSONString(obj))
        Set(path_to_list, obj)
    End If
    If obj Is List Then Return Set(RStrip(path_to_list, "/") & "/push", value)
    '-------------------------------------------------------------------------
    Return NotListError(obj)
End Sub

'Retrieves (and deletes) the object existing at the given location.
'WARNING: Performing this operation on an array will convert it into a list!
'Examples:
'<code>
''Items: [A, B, C, D]
''Persons: { John: { ... }, Kate: { ... } }
'Pop("/items/2") 'Gets element C
'Pop("/persons/john") 'Gets the 'John' object
''Items: [A, B, D]
''Persons: { Kate: { ... } }</code>
Public Sub Pop(path As String) As Object
    path = Strip(path.Trim, "/")
    Dim nodes() = Regex.Split("/", path) As String
    Dim parent_path As String
    Dim key_or_index As Object
    If nodes.Length == 1 Then
        If nodes(0).Length == 0 Then
            Dim obj = source As Object
            source = Null
            Return obj
        End If
        parent_path = "/"
    Else
        parent_path = path.SubString2(0, path.LastIndexOf("/"))
    End If
    key_or_index = nodes(nodes.Length - 1)
    Dim parent = Get(parent_path) As Object
    If IsArray(parent) Or parent Is List Then
        Return PopIndex(parent_path, key_or_index)
    End If
    Dim parent_map = parent As Map
    Dim obj = Get(path) As Object
    parent_map.Remove(key_or_index)
    Return obj
End Sub
#End Region

#Region Private
Private Sub JSONString(src As Object) As String
    Return RecursiveBuild(src, "")
End Sub

Private Sub RecursiveBuild(obj As Object, json As String) As String
    If obj == Null Then
        json = json & "null"
        Return json
    End If
    Dim t = GetType(obj).ToLowerCase As String
    Select True
        Case t == "[i"
            Dim a_i() = obj As Int
            json = json & "["
            For Each elm As Object In a_i
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[s"
            Dim a_s() = obj As Short
            json = json & "["
            For Each elm As Object In a_s
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[d"
            Dim a_d() = obj As Double
            json = json & "["
            For Each elm As Object In a_d
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[f"
            Dim a_f() = obj As Float
            json = json & "["
            For Each elm As Object In a_f
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[j"
            Dim a_l() = obj As Long
            json = json & "["
            For Each elm As Object In a_l
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[ljava.lang.string;"
            Dim a_str() = obj As String
            json = json & "["
            For Each elm As Object In a_str
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[ljava.lang.object;"
            Dim a_o() = obj As Object
            json = json & "["
            For Each elm As Object In a_o
                json = $"${json}${RecursiveBuild(elm, "")},"$
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[lanywheresoftware.b4a.objects.collections.map;"
            Dim a_map() = obj As Map
            json = json & "["
            For i = 0 To a_map.Length - 1
                If a_map(i).IsInitialized Then
                    json = $"${json}${RecursiveBuild(a_map(i), "")},"$
                Else
                    json = $"${json}{},"$
                End If
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "[lanywheresoftware.b4a.objects.collections.list;"
            Dim a_list() = obj As List
            json = json & "["
            For i = 0 To a_list.Length - 1
                If a_list(i).IsInitialized Then
                    json = $"${json}${RecursiveBuild(a_list(i), "")},"$
                Else
                    json = $"${json}[],"$
                End If
            Next
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case t == "java.lang.boolean"
            Dim s = obj As String
            json = $"${json}${s.ToLowerCase}"$

        Case t == "java.lang.string"
            Dim s = obj As String
            json = json & $""${s}""$

        Case t == "anywheresoftware.b4a.objects.collections.map$mymap"
            Dim m = obj As Map
            json = json & "{"
            If m.IsInitialized Then
                For Each key As Object In m.Keys
                    json = $"${json}"${key}": ${RecursiveBuild(m.Get(key), "")},"$
                Next
            End If
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "}"

        Case t == "java.util.arraylist"
            Dim v = obj As List
            json = json & "["
            If v.IsInitialized Then
                For Each elm As Object In v
                    json = $"${json}${RecursiveBuild(elm, "")},"$
                Next
            End If
            If json.EndsWith(",") Then json = json.SubString2(0, json.Length - 1)
            json = json & "]"

        Case                          _
            t == "java.lang.short",   _
            t == "java.lang.integer", _
            t == "java.lang.long",    _
            t == "java.lang.double",  _
            t == "java.lang.float"
            Dim s = obj As String
            json = json & s

        Case Else
            If IsArray(obj) Then
                t = t.SubString2(t.LastIndexOf("$") + 1, t.LastIndexOf(";"))
                Dim msg = $"Unsupported array type (${t})"$ As String
                ShowWarning(msg)
                json = json & $"["ERROR: ${msg}"]"$
                Exit
            End If
            Dim s = obj As String
            s = s.Replace(CRLF & "]", "}")
            s = "{" & s.SubString2(1, s.Length - 1) & "}"
            s = s.Replace("[IsInitialized", "{IsInitialized")
            s = Regex.Replace("[a-zA-Z0-9_.]+", s, Chr(34) & "$0" & Chr(34))
            s = s.Replace(Chr(34) & "=", Chr(34) & ":")
            s = s.Replace(":,", ":" & Chr(34) & Chr(34) & ",")
            s = s.Replace(":]", ":" & Chr(34) & Chr(34) & "]")
            s = s.Replace(":}", ":" & Chr(34) & Chr(34) & "}")
            json = json & s

    End Select
    Return json
End Sub

Private Sub GetObject(obj As Object, path As String) As Object
    path = Strip(path.Trim, "/")
    If path.Length == 0 Then Return obj
    '--------------------------------------------------------
    Dim keys() = Regex.Split("/", path) As String : path = ""
    Dim key = keys(0) As String
    For i = 1 To (keys.Length - 1)
        path = path & keys(i) & "/"
    Next
    If obj Is Map Then
        Dim map = obj As Map
        Dim next_obj = map.GetDefault(key, NOT_FOUND) As Object
        If next_obj == NOT_FOUND Then
            ShowWarning($"Key '${key}' not found - Null object returned."$)
            next_obj = Null
        End If
        obj = GetObject(next_obj, path)
    Else If obj Is List Then
        Dim lst = obj As List
        Dim idx = PyIndex(key, lst) As Int
        Try
            obj = GetObject(lst.Get(idx), path)
        Catch
            ShowWarning($"Index #${idx} out-of-bounds (list size is ${lst.Size}) - Null object returned."$)
            obj = Null
        End Try
    Else If IsArray(obj) Then
        Dim idx = PyIndex(key, lst) As Int
        obj = GetObject(GetArrayElement(obj, idx), path)
    End If
    Return obj
End Sub

Private Sub SetObject(obj As Object, path As String, value As Object) As Object
    path = Strip(path.Trim, "/")
    If path.Length == 0 Then
        source = value
        Return source
    End If
    '--------------------------------------------------------
    Dim keys() = Regex.Split("/", path) As String : path = ""
    Dim key = keys(0) As String
    For i = 1 To (keys.Length - 1)
        path = path & keys(i) & "/"
    Next
    If obj Is Map Then
        Dim map = obj As Map
        If keys.Length == 1 Then
            map.Put(key, value)
            Return map
        End If
        obj = SetObject(map.GetDefault(key, Null), path, value)
    Else If obj Is List Then
        Dim lst = obj As List
        If keys.Length == 1 Then
            If key == "push" Then
                lst.Add(value)
            Else _
            If key == "pop" Then
                Dim idx = PyIndex(value, lst) As Int
                Dim out = Null As Object
                Try
                    out = lst.Get(idx)
                    lst.RemoveAt(idx)
                Catch
                    ShowOutOfBoundsError(lst, idx)
                End Try
                Return out
            Else
                Dim idx = PyIndex(key, lst) As Int
                Try
                    lst.Set(idx, value)
                Catch
                    ShowOutOfBoundsError(lst, idx)
                End Try
            End If
            Return lst
        End If
        Try
            obj = SetObject(lst.Get(key), path, value)
        Catch
            obj = Null
        End Try
    End If
    Return obj
End Sub

Private Sub PopIndex(path As String, index As Int) As Object
    Dim obj = Get(path) As Object
    If IsArray(obj) Then
        obj = LoadString(JSONString(obj))
        Set(path, obj)
    End If
    Return Set(RStrip(path, "/") & "/pop", index)
End Sub

Private Sub RStrip(str As String, chars As String) As String
    Do While str.EndsWith(chars)
        str = str.SubString2(0, str.Length - chars.Length)
    Loop
    Return str
End Sub

Private Sub LStrip(str As String, chars As String) As String
    Do While str.StartsWith(chars)
        str = str.SubString(chars.Length)
    Loop
    Return str
End Sub

Private Sub Strip(str As String, chars As String) As String
    Return RStrip(LStrip(str, chars), chars)
End Sub

Private Sub PyIndex(index As Int, lst As List) As Int
    If index < 0 Then index = lst.Size + index
    Return index
End Sub

Private Sub LoadString(data As String) As Object
    data = data.Trim
    If data.Length == 0 Then Return Null
    Dim parser As JSONParser
    Select True
        Case data.StartsWith("{")
            parser.Initialize(data)
            Return parser.NextObject

        Case data.StartsWith("[")
            parser.Initialize(data)
            Return parser.NextArray

        Case Else
            Return data

    End Select
End Sub

Private Sub NotListError(obj As Object) As Object
    Try
        Dim t = GetType(obj) As String
        If t.StartsWith("[") Then
            t = "Array"
        Else If obj Is Map Then
            t = "Map"
        Else
            t = t.SubString(t.LastIndexOf(".") + 1)
        End If
    Catch
        t = "Null"
    End Try
    ShowError($"Target object (${t}) is not a List!"$)
    If Not(IgnoreErrors) Then ExitApplication2(1)
    Return Null
End Sub

Private Sub ShowOutOfBoundsError(v As List, i As Int) As Object
    ShowError($"Index #${i} out-of-bounds (list size is ${v.Size})!"$)
    If Not(IgnoreErrors) Then ExitApplication2(1)
    Return Null
End Sub

Private Sub ShowWarning(str As String) As Object
    If Not(ShowWarnings) Then Return Null
    LogError($"[WARNING] NinjaData: ${str}"$)
    Return Null
End Sub

Private Sub ShowError(str As String) As Object
    LogError($"[ERROR] NinjaData: ${str}"$)
    Return Null
End Sub

Private Sub IsArray(obj As Object) As Boolean
    Return obj <> Null And GetType(obj).StartsWith("[")
End Sub

Private Sub GetArrayElement(arr As Object, index As Int) As Object
    Dim t = GetType(arr).ToLowerCase As String
    Select t
        Case "[i"
            Dim a_i() = arr As Int
            Return a_i(index)

        Case "[s"
            Dim a_s() = arr As Short
            Return a_s(index)

        Case "[d"
            Dim a_d() = arr As Double
            Return a_d(index)

        Case "[f"
            Dim a_f() = arr As Float
            Return a_f(index)

        Case "[j"
            Dim a_l() = arr As Long
            Return a_l(index)

        Case "[ljava.lang.string;"
            Dim a_str() = arr As String
            Return a_str(index)

        Case "[ljava.lang.object;"
            Dim a_o() = arr As Object
            Return a_o(index)

        Case "[lanywheresoftware.b4a.objects.collections.map;"
            Dim a_map() = arr As Map
            Return a_map(index)

        Case "[lanywheresoftware.b4a.objects.collections.list;"
            Dim a_list() = arr As List
            Return a_list(index)

    End Select
    ShowWarning($"Unknown array type (${t}) - Null object returned."$)
    Return Null
End Sub
#End Region
